<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::where('content', '!=', '')->latest()->get();
        $name = [];
        foreach ($posts as $post) {
            $name[] = User::where('id', $post->user_id)->first()->name;
        }
        return view('post.feeds', compact('posts', 'name'));
    }

    public function addPost(Request $request)
    {
        $path='';
        if (isset($_FILES['image']['name'])) {
            $file = $request->file('image');
            $dest_path = './';
            $file->move($dest_path, $file->getClientOriginalName());
//            echo "knfkdf";
            $path=$dest_path.$_FILES['image']['name'];
        }
        Post::create([
            'user_id' => Auth::user()->id,
            'content' => $request->contents,
            'file_src'=>$path,
        ]);
        return redirect('/');
    }
}
