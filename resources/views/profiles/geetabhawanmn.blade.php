<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- SITE TITLE -->
    <title>Blessd.in</title>
    <!-- Latest Bootstrap min CSS -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/css/bootstrap.min.css">
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:200,400,300,600,700|Raleway:300,400,500,600,700'
          rel='stylesheet' type='text/css'>
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/fonts/font-awesome.min.css">
    <!--- owl carousel Css-->
    <link rel="stylesheet" href="../html-preview/keeway/assets/owlcarousel/css/owl.carousel.css">
    <link rel="stylesheet" href="../html-preview/keeway/assets/owlcarousel/css/owl.theme.css">
    <!-- venobox CSS -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/css/venobox.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/css/animate.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/css/style.css">
    <!--<link rel="stylesheet" href="../html-preview/keeway/assets/css/main.css">-->
    <!--<link rel="stylesheet" href="../html-preview/keeway/assets/css/custom-component.css">-->
    <link rel="stylesheet" href="../html-preview/keeway/assets/css/responsive.css">
    <!-- CSS FOR COLOR SWITCHER -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/css/switcher/switcher.css">
    <!--<link rel="stylesheet" href="../html-preview/keeway/assets/css/switcher/style1.css" id="colors">		-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .close {
            color: black;
            position: relative;
            top: 0px;
            right: 25px;
            font-size: 35px;
            font-weight: bold;
        }

        .fixme {
            z-index: 555;
            vertical-align: center;
            margin-left: 1%;
            text-align: center;

            height: 14%;
        }

        .navbar .navbar-default div.container {
            margin-left: 0px !important;
        }

        @media screen and (max-device-width: 375px) {
            .footer_content {
                font-size: 11px;
            }

            .fixme {
                height: 14%;
            }

        }

        .btn-home-border {

            border-color: #03a9f4;
            color: black;
        }

        .navbar-default .navbar-nav > li > a {
            color: #ffffff;
        }

        /* Create four equal columns that floats next to eachother */
        .column {
            float: left;
            width: 25%;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            position: fixed;
            z-index: 600;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: none;
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            width: 90%;
            max-width: 1200px;
        }

        /* The Close Button */

        .close:hover,
        .close:focus {
            color: #999;
            text-decoration: none;
            cursor: pointer;
        }

        /* Hide the slides by default */
        .mySlides {
            display: none;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* Caption text */
        .caption-container {
            text-align: center;
            background-color: black;
            padding: 2px 16px;
            color: white;
        }

        img.demo {
            opacity: 0.6;
        }

        .active,
        .demo:hover {
            opacity: 1;
        }

        img.hover-shadow {
            transition: 0.3s
        }

        .hover-shadow:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
        }


    </style>
</head>

<body data-spy="scroll" data-offset="80">
<!-- Latest jQuery -->
<script src="../html-preview/keeway/assets/js/jquery-1.12.4.min.js"></script>
<!-- Latest compiled and minified Bootstrap -->
<script src="../html-preview/keeway/assets/js/bootstrap.min.js"></script>
<!-- modernizr JS -->
<script src="../html-preview/keeway/assets/js/modernizr-2.8.3.min.js"></script>
<!-- stellar js -->
<script src="../html-preview/keeway/assets/js/jquery.stellar.min.js"></script>
<!-- countTo js -->
<script src="../html-preview/keeway/assets/js/jquery.inview.min.js"></script>
<!-- owl-carousel min js  -->
<script src="../html-preview/keeway/assets/owlcarousel/js/owl.carousel.min.js"></script>
<!-- jquery mixitup min js -->
<script src="../html-preview/keeway/assets/js/jquery.mixitup.js"></script>
<!-- jquery venobox min js -->
<script src="../html-preview/keeway/assets/js/venobox.min.js"></script>
<!-- scrolltopcontrol js -->
<script src="../html-preview/keeway/assets/js/scrolltopcontrol.js"></script>
<!-- form-contact js -->
<script src="../html-preview/keeway/assets/js/form-contact.js"></script>
<!-- jquery appear js -->
<script src="../html-preview/keeway/assets/js/jquery.appear.js"></script>
<!-- WOW - Reveal Animations When You Scroll -->
<script src="../html-preview/keeway/assets/js/wow.min.js"></script>
<!-- switcher.js -->
<script src="../html-preview/keeway/assets/js/switcher.js"></script>
<!-- scripts js -->
<script src="../html-preview/keeway/assets/js/scripts.js"></script>
<!-- START PRELOADER -->
<div class="preloader">
    <div class="status">
        <div class="status-mes"><h4>Keeway</h4></div>
    </div>
</div>
<!-- END PRELOADER -->

<!-- START NAVBAR -->

<div class="navbar navbar-default" style="background: #03a9f4">

    <div style="padding-left: 40px;">
        <div class="navbar-collapse collapse ">
            <nav class="navbar">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="/" class="navbar-brand"><h1 class="zero-top-margin color4"
                                                             style="padding-right: 15px;color: #ffffff">Blessd.in</h1>
                        </a>
                    </li>
                    <li style="width: 250px;"><a>
                            <select class="form-control">
                                <option><i class="fa fa-html5"></i>New Delhi</option>
                            </select>
                        </a></li>
                    <li style="width: 400px;"><a>
                            <input type="text" class="form-control" style="width: 100%;" placeholder="Search Temples">
                        </a></li>

                    <li><a href="#contact">
                            <button class="btn btn-default btn-light-bg" style="border-color: #2193d6fc">Search</button>
                        </a></li>
                    <!--<li><a href="#contact">Contact</a></li>-->
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#contact">Login/Register</a></li>

                </ul>
            </nav>
        </div>
    </div>
</div>


<div class=" navbar navbar-default fixme">

    <div class="row">


        <div class="container">
            <div class="col-md-5 col-md-offset-3 col-sm-6  text-center">
                <h2 class="headings color4">Geeta Bhawan Mandir</h2>
            </div>

            <div class="col-md-1 col-md-offset-1 hidden-xs col-sm-offset-2">
                <button class="btn btn-default btn-home-border" style="border-color: #03a9f4">Follow</button>
            </div>
            <div class="col-md-1 col-md-offset-1 hidden-xs">
                <div class="row">
                    <button class="btn btn-default btn-home-border" style="border-color:#03a9f4">Donate Now</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-md-offset-3 col-sm-6 text-center">
            <h2 class="headings color4 subheading">Malviya Nagar</h2>
        </div>
    </div>


</div>
<!-- END NAVBAR -->

<!-- START HOME DESIGN -->

<!-- START HOME -->
<div class="container" style="padding-top: 100px;">
    <section id="home" class="welcome-area">
        <div class="welcome-slider-area">
            <div id="welcome-slide-carousel" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="single-slide-item slide-1">
                            <div class="single-slide-item-table">
                                <div class="single-slide-item-tablecell">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <br><br><br><br><br><br>
                                                <h2>My Temple is where I find peace</h2>
                                                <p class="lead"
                                                   style="-webkit-text-stroke-color:black;-webkit-text-stroke-width:.2px">
                                                    <b>There is a feeling of inner peace when I visit the temple and
                                                        pray to god with my eyes closed</b></p>

                                                <a class="btn btn-default btn-home-border page-scroll" href="#"
                                                   style="font-size: 16px; font-weight: bold;"><b>Donate
                                                        Now</b></a>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <img src="../html-preview/keeway/assets/img/donation1.png" width="8%"
                                                 height="8%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="single-slide-item slide-2">
                            <div class="single-slide-item-table">
                                <div class="single-slide-item-tablecell">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <br><br><br><br><br><br>
                                                <h2>Making a Difference</h2>
                                                <p class="lead"
                                                   style="-webkit-text-stroke-color:black;-webkit-text-stroke-width:.2px">
                                                    <b>Geeta Bhawan Mandir Trust aims to take a proactive approach in
                                                        contributing to social initiatives.</b></p>

                                                <a class="btn btn-default btn-home-border page-scroll" href="#"
                                                   style="font-size: 16px; font-weight: bold;"><b>Donate
                                                        Now</b></a>

                                            </div>
                                        </div>
                                        <div class='row'>
                                            <img src="../html-preview/keeway/assets/img/donation1.png" width="8%"
                                                 height="8%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#welcome-slide-carousel" role="button" data-slide="prev">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </a>
                <a class="right carousel-control" href="#welcome-slide-carousel" role="button" data-slide="next">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </section>
    <!-- END  HOME DESIGN -->
    <!-- START FEATURE-->
    <section id="about" class="features">
        <div class="container">
            <div class="row text-center">
                <div class="section-title text-center wow zoomIn">
                    <h2><span>Community</span> Services</h2>
                    <div class="sec_icon"><span></span><span></span><span></span></div>
                </div>

                <div class="col-md-1 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0">

                </div><!--- END COL -->
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0" style="left:50px;">
                    <div class="single_about">
                        <img src="../html-preview/keeway/assets/img/work/Matrimony.jpg"
                             class="img-circle" onclick="openModal(3);currentSlide(1,3)" alt="Cinque Terre" width="200"
                             height="130">
                        <h4>Matrimony</h4>
                        <p>Geeta Bhawan Mandir, Malviya Nagar offers Matrimony services in partnership with Hans!
                            helping hundreds of people find their match.</p>
                    </div>
                </div><!--- END COL -->

                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0" style="left:50px;">
                    <div class="single_about">
                        <img src="../html-preview/keeway/assets/img/work/Physio3.jpg"
                             class="img-circle" onclick="openModal(1);currentSlide(1,1)" alt="Cinque Terre" width="200"
                             height="130">
                        <h4>Physiotherapy</h4>
                        <p> Geeta Bhawan Mandir, Malviya Nagar offers Physiotherapy services for the elderly helping
                            hundreds of people who can’t afford an expensive private center</p>
                    </div>
                </div><!--- END COL -->

                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0" style="left:50px;">
                    <div class="single_about">
                        <img src="../html-preview/keeway/assets/img/work/Allopathy3.jpg"
                             class="img-circle" onclick="openModal(2);currentSlide(1,2)" alt="Cinque Terre" width="200"
                             height="130">
                        <h4>Allopathy</h4>
                        <p>Geeta Bhawan Mandir, Malviya Nagar offers facility to provide service of checking your health
                            and provide allopathic medicines at a nominal price</p>
                    </div>
                </div><!--- END COL -->

                <!--  <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                      data-wow-offset="0">
                     <div class="single_about">
                         <img src="http://church-event.vamtam.com/wp-content/uploads/2013/03/about_4.jpg"
                              class="img-circle" alt="Cinque Terre">

                         <h4>Book a Bhandara/Langar</h4>
                         <p> Geeta Bhawan Mandir, Malviya Nagar offers a facility to contribute to a Bhandara/Langar that
                             occurs every Saturday, offering food to the poor and needy</p>
                     </div>
                 </div><! --><!-- --- END COL -- -->
            </div><!--- END ROW -->
        </div><!--- END CONTAINER -->
    </section>

    <div id="myModal1" class="modal">
        <span class="close cursor" onclick="closeModal(1)">&times;</span>
        <div class="modal-content">
            <div class="mySlides1">
                <div class="numbertext">1 / 3</div>
                <img src="../html-preview/keeway/assets/img/work/Physio1.jpg" style="width:100%">
            </div>

            <div class="mySlides1">
                <div class="numbertext">2 / 3</div>
                <img src="../html-preview/keeway/assets/img/work/Physio2.jpg" style="width:100%">
            </div>

            <div class="mySlides1">
                <div class="numbertext">3 / 3</div>
                <img src="../html-preview/keeway/assets/img/work/Physio3.jpg" style="width:100%">
            </div>

            <!-- Next/previous controls -->
            <a class="prev" onclick="plusSlides(-1,1)" style="left:0;">&#10094;</a>
            <a class="next" onclick="plusSlides(1,1)">&#10095;</a>

            <!-- Caption text -->
            <div class="caption-container">
                <p id="caption1">Geeta Bhawan Mandir, Malviya Nagar offers Physiotherapy services for the elderly
                    helping
                    hundreds of people who can’t afford an expensive private center</p>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal">
        <span class="close cursor" onclick="closeModal(2)">&times;</span>
        <div class="modal-content">
            <div class="mySlides2">
                <div class="numbertext">1 / 3</div>
                <img src="../html-preview/keeway/assets/img/work/Allopathy1.jpg" style="width:100%">
            </div>

            <div class="mySlides2">
                <div class="numbertext">2 / 3</div>
                <img src="../html-preview/keeway/assets/img/work/Allopathy2.jpg" style="width:100%">
            </div>

            <div class="mySlides2">
                <div class="numbertext">3 / 3</div>
                <img src="../html-preview/keeway/assets/img/work/Allopathy3.jpg" style="width:100%">
            </div>

            <!-- Next/previous controls -->
            <a class="prev" onclick="plusSlides(-1,2)" style="left:0;">&#10094;</a>
            <a class="next" onclick="plusSlides(1,2)">&#10095;</a>

            <!-- Caption text -->
            <div class="caption-container">
                <p id="caption2">Geeta Bhawan Mandir, Malviya Nagar offers facility to provide service of checking your
                    health and provide allopathic medicines at a nominal price</p>
            </div>
        </div>
    </div>


    <div id="myModal3" class="modal">
        <span class="close cursor" onclick="closeModal(3)">&times;</span>
        <div class="modal-content">
            <div class="mySlides3">
                <div class="numbertext">1 / 2</div>
                <img src="../html-preview/keeway/assets/img/work/Matrimony1.jpeg" style="width:100%">
            </div>

            <div class="mySlides3">
                <div class="numbertext">2 / 2</div>
                <img src="../html-preview/keeway/assets/img/work/Matrimony2.jpg" style="width:100%">
            </div>

            <!-- <div class="mySlides1">
              <div class="numbertext">3 / 3</div>
              <img src="../html-preview/keeway/assets/img/work/Physio3.jpg" style="width:100%">
            </div> -->

            <!-- Next/previous controls -->
            <a class="prev" onclick="plusSlides(-1,3)" style="left:0;">&#10094;</a>
            <a class="next" onclick="plusSlides(1,3)">&#10095;</a>

            <!-- Caption text -->
            <div class="caption-container">
                <p id="caption3">Geeta Bhawan Mandir, Malviya Nagar offers Matrimony services in partnership with Hans!
                    helping hundreds of people find their match.</p>
            </div>
        </div>
    </div>

    <section id="about" class="features">
        <div class="container">
            <div class="row text-center">
                <div class="section-title text-center wow zoomIn">
                    <h2><span>Online</span> Booking</h2>
                    <div class="sec_icon"><span></span><span></span><span></span></div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0">
                    <div class="single_about">
                        <img src="../html-preview/keeway/assets/img/work/pooja.jpg"
                             class="img-circle" alt="Cinque Terre" width="200" height="130">
                        <h4>Book a Pooja</h4>
                        <p>Geeta Bhawan Mandir, Malviya Nagar offers book a pooja.</p>
                    </div>
                </div><!--- END COL -->

                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0">
                    <div class="single_about">
                        <img src="http://church-event.vamtam.com/wp-content/uploads/2013/03/about_4.jpg"
                             class="img-circle" alt="Cinque Terre">
                        <h4>Book a Bhandara/Langar</h4>
                        <p> Geeta Bhawan Mandir, Malviya Nagar offers a facility to contribute to a Bhandara/Langar that
                            occurs every Saturday, offering food to the poor and needy</p>
                    </div>
                </div><!--- END COL -->

                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0">
                    <div class="single_about">
                        <img src="../html-preview/keeway/assets/img/work/Room.jpeg"
                             class="img-circle" alt="Cinque Terre" width="200" height="130">
                        <h4>Hall&Room Booking</h4>
                        <p>Geeta Bhawan Mandir, Malviya Nagar offers facility to book their Hall for event and functions
                            such as Birthday, Marriages, etc. They also offer stay in their rooms at a nominal price</p>
                    </div>
                </div><!--- END COL -->

                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s"
                     data-wow-offset="0">
                    <div class="single_about">
                        <img src="../html-preview/keeway/assets/img/work/darshan.jpg"
                             class="img-circle" alt="Cinque Terre" width="200" height="130">

                        <h4>Darshan</h4>
                        <p> Geeta Bhawan Mandir, Malviya Nagar offers to book darshan of the temple online.</p>
                    </div>
                </div><!--- END COL -->
            </div><!--- END ROW -->
        </div><!--- END CONTAINER -->
    </section>

    <section id="event" class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="content-text">
                        <h3 class="wow fadeInUp animated" data-wow-delay="0.8s"
                            style="visibility: visible;-webkit-animation-delay: 0.8s; -moz-animation-delay: 0.8s; animation-delay: 0.8s;">
                            <div class="section-title text-center wow zoomIn">
                                <h2 style="color:white;"><span>Upcoming</span> Events</h2>
                                <div class="sec_icon"><span></span><span></span><span></span></div>
                            </div>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="tab-block wow fadeInDown animated" data-wow-delay="0s"
                         style="visibility: visible;-webkit-animation-delay: 0s; -moz-animation-delay: 0s; animation-delay: 0s;">
                        <div class="icon">
                            <i class="fa  fa-calendar"></i>
                        </div>
                        <div class="desc">
                            <h2 style="color: white">28 March</h2>
                            <h3 style="color: white">Holi Event</h3>
                            <p>At Geeta Bhawan Mandir</p>
                            <br>
                            <p>
                                <button class="btn btn-default btn-light-bg">Read More</button>
                            </p>
                        </div>
                    </div>
                </div>

                <!--<div class="col-md-3 col-sm-6 col-xs-12">-->
                <!--<div class="tab-block wow fadeInDown animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">-->
                <!--<div class="icon">-->
                <!--<i class="icon-calendar"></i>-->
                <!--</div>-->
                <!--<div class="desc">-->
                <!--<h4>Date &amp; Time</h4>-->
                <!--<p>1PM - 11PM, 25- 28 Jan</p>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-3 col-sm-6 col-xs-12">-->
                <!--<div class="tab-block wow fadeInDown animated" data-wow-delay="0.4s" style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">-->
                <!--<div class="icon">-->
                <!--<i class="icon-microphone"></i>-->
                <!--</div>-->
                <!--<div class="desc">-->
                <!--<h4>Speakers</h4>-->
                <!--<p>10 Tech Icons and Professionals</p>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-3 col-sm-6 col-xs-12">-->
                <!--<div class="tab-block last-block wow fadeInDown animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">-->
                <!--<div class="icon">-->
                <!--<i class="icon-wallet"></i>-->
                <!--</div>-->
                <!--<div class="desc left">-->
                <!--<h4>Tickets</h4>-->
                <!--<p>150 People</p>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->

            </div>
        </div>
    </section>
    <!-- END FEATURE -->
    <!-- START ABOUT US -->
    <!--<section class="about_us section-padding">-->
    <!--<div class="container">-->
    <!--<div class="row">-->
    <!--<div class="section-title text-center wow zoomIn">-->
    <!--<h2>Our <span>Skills</span></h2>-->
    <!--<div class="sec_icon"><span></span><span></span><span></span></div>-->
    <!--</div>-->

    <!--<div class="col-sm-6 col-xs-12">-->
    <!--<div class="skill_content">-->
    <!--<h3>What We Do</h3>-->
    <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting-->
    <!--industry. Lorem Ipsum has been the industry's standard dummy text-->
    <!--ever since the 1500s, when an unknown printer.</p>-->
    <!--<ul class="skill_list">-->
    <!--<li><i class='fa fa-check'></i> Design</li>-->
    <!--<li><i class='fa fa-check'></i> Development</li>-->
    <!--<li><i class='fa fa-check'></i> Marketing</li>-->
    <!--<li><i class='fa fa-check'></i> Photography</li>-->
    <!--</ul>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;-->

    <!--<div class="col-sm-6 col-xs-12 ">-->
    <!--<div class="skills">-->
    <!--<div class="progress-bar-linear">-->
    <!--<p class="progress-bar-text">design</p>-->
    <!--<div class="progress-bar">-->
    <!--<span data-percent="95"></span>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div class="progress-bar-linear">-->
    <!--<p class="progress-bar-text">Development </p>-->
    <!--<div class="progress-bar">-->
    <!--<span data-percent="90"></span>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div class="progress-bar-linear">-->
    <!--<p class="progress-bar-text">Marketing </p>-->
    <!--<div class="progress-bar">-->
    <!--<span data-percent="75"></span>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div class="progress-bar-linear">-->
    <!--<p class="progress-bar-text">photography</p>-->
    <!--<div class="progress-bar">-->
    <!--<span data-percent="75"></span>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END ROW &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END CONTAINER &ndash;&gt;-->
    <!--</section>-->
    <!--END ABOUT US-->
    <!--START WHY CHOOSE US-->
    <!--<section class="why_choose_us section-padding">-->
    <!--<div class="container">-->
    <!--<div class="row">-->
    <!--<div class="section-title text-center wow zoomIn">-->
    <!--<h2>Why <span>choose</span> us</h2>-->
    <!--<div class="sec_icon"><span></span><span></span><span></span></div>-->
    <!--</div>-->
    <!--<div class="col-md-6 col-sm-6 col-xs-12">-->
    <!--<div id="why_choose">-->
    <!--&lt;!&ndash; Wrapper for slides &ndash;&gt;-->
    <!--<div class="single_feature">-->
    <!--<div class="feat_number">01</div>-->
    <!--<div class="single_f_content">-->
    <!--<h4>Unique Ideas</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet euismod. Curabitur accumsan vitae</p>-->
    <!--</div>-->
    <!--</div>-->

    <!--<div class="single_feature">-->
    <!--<div class="feat_number">02</div>-->
    <!--<div class="single_f_content">-->
    <!--<h4>Timely delivery</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet euismod. Curabitur accumsan vitae</p>-->
    <!--</div>-->
    <!--</div>-->

    <!--<div class="single_feature">-->
    <!--<div class="feat_number">03</div>-->
    <!--<div class="single_f_content">-->
    <!--<h4>Cost effectiveness</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet euismod. Curabitur accumsan vitae</p>-->
    <!--</div>-->
    <!--</div>-->

    <!--</div>&lt;!&ndash; END CAROUSEL SLIDE &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;-->
    <!--<div class="col-md-6 col-sm-6 col-xs-12">-->
    <!--<div class="feature_img">-->
    <!--<img src="../html-preview/keeway/assets/img/laptop.png" class="img-responsive" alt="" />-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END ROW &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END CONTAINER &ndash;&gt;-->
    <!--</section>-->
    <!--&lt;!&ndash; END WHY CHOOSE US &ndash;&gt;-->

    <!--&lt;!&ndash; START SERVICE  &ndash;&gt;-->
    <!--<section id="service" class="our_service section-padding">-->
    <!--<div class="container">-->
    <!--<div class="row text-center">-->
    <!--<div class="section-title wow zoomIn">-->
    <!--<h2>our <span>service</span></h2>-->
    <!--<div class="sec_icon"><span></span><span></span><span></span></div>-->
    <!--</div>-->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">-->
    <!--<div class="single_service">-->
    <!--<i class="fa fa-circle-o-notch"></i>-->
    <!--<div class="ser_content">-->
    <!--<h4>Consultancy</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet.</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;END COL &ndash;&gt;-->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">-->
    <!--<div class="single_service">-->
    <!--<i class="fa fa-keyboard-o"></i>-->
    <!--<div class="ser_content">-->
    <!--<h4>Finance</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet.</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;END COL &ndash;&gt;-->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">-->
    <!--<div class="single_service">-->
    <!--<i class="fa fa-bullhorn"></i>-->
    <!--<div class="ser_content">-->
    <!--<h4>Advertising</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet.</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;END COL &ndash;&gt;-->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">-->
    <!--<div class="single_service">-->
    <!--<i class="fa fa-life-bouy"></i>-->
    <!--<div class="ser_content">-->
    <!--<h4>Planning</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet.</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;END COL &ndash;&gt;-->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">-->
    <!--<div class="single_service">-->
    <!--<i class="fa fa-briefcase"></i>-->
    <!--<div class="ser_content">-->
    <!--<h4>Online Support</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet.</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;END COL &ndash;&gt;-->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">-->
    <!--<div class="single_service">-->
    <!--<i class="fa fa-camera-retro"></i>-->
    <!--<div class="ser_content">-->
    <!--<h4>Super Ideas</h4>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum ante vel aliquet.</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;END COL &ndash;&gt;-->
    <!--</div>&lt;!&ndash;END  ROW &ndash;&gt;-->
    <!--</div>&lt;!&ndash; END CONTAINER &ndash;&gt;-->
    <!--</section>-->
    <!--&lt;!&ndash; END SERVICE&ndash;&gt;-->

    <!--&lt;!&ndash; START PORTFOLIO &ndash;&gt;-->
    <section id="portfolio" class="works_area section-padding">
        <div class="container">
            <div class="row text-center">
                <div class="section-title wow zoomIn">
                    <h2>Our <span>Team</span></h2>
                    <div class="sec_icon"><span></span><span></span><span></span></div>
                </div>
                <div class="col-md-12">
                    <div class="our_work_menu">
                        <ul>
                            <li class="filter wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".25s"
                                data-filter=".management">Management Committee
                            </li>

                            <li class="filter" data-filter=".priests">Priests</li>
                            <!--<li class="filter" data-filter=".branding">Branding</li>-->
                            <!--<li class="filter" data-filter=".web">Web</li>-->
                            <!--<li class="filter" data-filter=".package">Package</li>-->
                            <!--<li class="filter" data-filter=".video">Video</li>-->
                        </ul>
                    </div>
                </div>

                <div class="work_all_item" style="width: 80%;">
                    <div class="grid-item col-md-3 col-sm-6 col-xs-12 mix all management">
                        <div class="single_team">
                            <div class="img_wrap">
                                <img src="../html-preview/keeway/assets/img/team/1.png" class="img-responsive" alt=""/>
                                <div class="social_link">
                                    <h3>Joe S. Sloat</h3>
                                    <p>Co Founder</p>

                                    <ul class="list-inline">
                                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="work_all_item" style="width: 80%;">
                    <div class="grid-item col-md-3 col-sm-6 col-xs-12 mix all priests">
                        <div class="single_team">
                            <div class="img_wrap">
                                <img src="../html-preview/keeway/assets/img/team/2.png" class="img-responsive" alt=""/>
                                <div class="social_link">
                                    <h3>Joe S. Sloat</h3>
                                    <p>Co Founder</p>

                                    <ul class="list-inline">
                                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="work_all_item" style="width: 80%;">
                    <div class="grid-item col-md-3 col-sm-6 col-xs-12 mix all management">
                        <div class="single_team">
                            <div class="img_wrap">
                                <img src="../html-preview/keeway/assets/img/team/3.png" class="img-responsive" alt=""/>
                                <div class="social_link">
                                    <h3>Joe S. Sloat</h3>
                                    <p>Co Founder</p>

                                    <ul class="list-inline">
                                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="work_all_item" style="width: 80%;">
                    <div class="grid-item col-md-3 col-sm-6 col-xs-12 mix all priests">
                        <div class="single_team">
                            <div class="img_wrap">
                                <img src="../html-preview/keeway/assets/img/team/4.png" class="img-responsive" alt=""/>
                                <div class="social_link">
                                    <h3>Joe S. Sloat</h3>
                                    <p>Co Founder</p>

                                    <ul class="list-inline">
                                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- END PORTFOLIO -->
    <!-- START COUNDOWN -->
    <!--<section class="counter_feature">-->
    <!--<div class="container">-->
    <!--<div class="row">-->
    <!--<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"-->
    <!--data-wow-offset="0">-->
    <!--<div class="counter">-->
    <!--<h2 class="timer"> 2500</h2>-->
    <!--<h5>Projects Finished</h5>-->
    <!--</div>-->
    <!--</div> &lt;!&ndash;- END COL &ndash;&gt;-->
    <!--<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s"-->
    <!--data-wow-offset="0">-->
    <!--<div class="counter">-->
    <!--<h2 class="timer">28256 </h2>-->
    <!--<h5>Line Of Coding</h5>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;-->
    <!--<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s"-->
    <!--data-wow-offset="0">-->
    <!--<div class="counter">-->
    <!--<h2 class="timer"> 1296</h2>-->
    <!--<h5>Award Won</h5>-->
    <!--</div>-->
    <!--</div> &lt;!&ndash;- END COL &ndash;&gt;-->
    <!--<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s"-->
    <!--data-wow-offset="0">-->
    <!--<div class="counter">-->
    <!--<h2 class="timer">2000</h2>-->
    <!--<h5>Satisfied Clients</h5>-->
    <!--</div>-->
    <!--</div> &lt;!&ndash;- END COL &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END ROW &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END CONTAINER &ndash;&gt;-->
    <!--</section>-->
    <!--END COUNDOWN-->

    <!--START ABOUT US-->
    <section data-stellar-background-ratio="0.3" id="video" class="about_video section-padding"
             style="background-image: url(../html-preview/keeway/assets/img/bg/bg-2.jpg); background-size:cover; background-position: center center;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="video-container">
                        <a data-toggle="modal" data-target="#video-modal" data-backdrop="true" class="modal_icon">
                            <span class="play-video"><span class="fa fa-play"></span></span></a>
                        <h4>Watch Video</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy <br>text ever since the 1500s, when an unknown printer.
                        </p>
                    </div>
                    <!-- VIDEO POPUP -->
                    <div class="modal fade video-modal" id="video-modal" role="dialog">
                        <div class="modal-content">
                            <iframe width="712" height="400" src="https://www.youtube.com/watch?v=vrBjDuWEhWI"></iframe>
                        </div>
                    </div>
                    <!-- END VIDEO POPUP -->
                </div><!--- END COL -->
            </div><!--- END ROW -->
        </div><!--- END CONTAINER -->
    </section>
    <!-- END ABOUT US -->


    <!-- START OUR TEAM -->
    <!--<section id="team" class="our_team section-padding">-->
    <!--<div class="container">-->
    <!--<div class="row text-center">-->
    <!--<div class="section-title wow zoomIn">-->
    <!--<h2>Management</h2>-->
    <!--<div class="sec_icon"><span></span><span></span><span></span></div>-->
    <!--</div>-->
    <!--<div class="col-md-3 col-sm-6">-->
    <!--<div class="single_team">-->
    <!--<div class="img_wrap">-->
    <!--<img src="../html-preview/keeway/assets/img/team/1.png" class="img-responsive" alt=""/>-->
    <!--<div class="social_link">-->
    <!--<h3>Joe S. Sloat</h3>-->
    <!--<p>Co Founder</p>-->

    <!--<ul class="list-inline">-->
    <!--<li><a href=""><i class="fa fa-facebook"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-twitter"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-linkedin"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-google-plus"></i></a></li>-->
    <!--</ul>-->
    <!--</div>-->
    <!--</div>-->

    <!--</div>-->
    <!--</div> &lt;!&ndash; End Col &ndash;&gt;-->
    <!--<div class="col-md-3 col-sm-6">-->
    <!--<div class="single_team">-->
    <!--<div class="img_wrap">-->
    <!--<img src="../html-preview/keeway/assets/img/team/2.png" class="img-responsive" alt=""/>-->
    <!--<div class="social_link">-->
    <!--<h3>Joe S. Sloat</h3>-->
    <!--<p>Co Founder</p>-->

    <!--<ul class="list-inline">-->
    <!--<li><a href=""><i class="fa fa-facebook"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-twitter"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-linkedin"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-google-plus"></i></a></li>-->
    <!--</ul>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div> &lt;!&ndash; End Col &ndash;&gt;-->
    <!--<div class="col-md-3 col-sm-6">-->
    <!--<div class="single_team">-->
    <!--<div class="img_wrap">-->
    <!--<img src="../html-preview/keeway/assets/img/team/3.png" class="img-responsive" alt=""/>-->
    <!--<div class="social_link">-->
    <!--<h3>Joe S. Sloat</h3>-->
    <!--<p>Co Founder</p>-->

    <!--<ul class="list-inline">-->
    <!--<li><a href=""><i class="fa fa-facebook"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-twitter"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-linkedin"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-google-plus"></i></a></li>-->
    <!--</ul>-->
    <!--</div>-->
    <!--</div>-->

    <!--</div>-->
    <!--</div> &lt;!&ndash; End Col &ndash;&gt;-->
    <!--<div class="col-md-3 col-sm-6">-->
    <!--<div class="single_team">-->
    <!--<div class="img_wrap">-->
    <!--<img src="../html-preview/keeway/assets/img/team/4.png" class="img-responsive" alt=""/>-->
    <!--<div class="social_link">-->
    <!--<h3>Joe S. Sloat</h3>-->
    <!--<p>Co Founder</p>-->

    <!--<ul class="list-inline">-->
    <!--<li><a href=""><i class="fa fa-facebook"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-twitter"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-linkedin"></i></a></li>-->
    <!--<li><a href=""><i class="fa fa-google-plus"></i></a></li>-->
    <!--</ul>-->
    <!--</div>-->
    <!--</div>-->

    <!--</div>-->
    <!--</div> &lt;!&ndash; End Col &ndash;&gt;-->
    <!--</div>-->
    <!--</div>-->
    <!--</section>-->
    <!-- END TEAM -->


    <!-- START NEWSLETTER -->

    <!-- END NEWSLETTER -->
    <!-- START PRICING -->

    <!-- END PRICING -->
    <!-- Start testimonials -->

    <!-- End testimonials -->

    <!-- START COMPANY PARTNER LOGO  -->
    <!--<div class="partner-logo ">-->
    <!--<div class="partner_overlay">-->
    <!--<div class="container">-->
    <!--<div class="row">-->
    <!--<div class="col-md-12">-->
    <!--<div class="partner wow fadeInRight">-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/1.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/2.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/3.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/4.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/5.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/6.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/1.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/2.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/3.png" alt="image"></a>-->
    <!--<a href="#" target="_blank"><img src="../html-preview/keeway/assets/img/partner/4.png" alt="image"></a>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash; END COL  &ndash;&gt;-->
    <!--</div>&lt;!&ndash;END  ROW  &ndash;&gt;-->
    <!--</div>&lt;!&ndash; END CONTAINER  &ndash;&gt;-->
    <!--</div>&lt;!&ndash; END OVERLAY &ndash;&gt;-->
    <!--</div>-->
    <!--&lt;!&ndash; END COMPANY PARTNER LOGO &ndash;&gt;-->
    <!--&lt;!&ndash; START HOME BLOG &ndash;&gt;-->
    <!--<section id="blog" class="home_blog section-padding">-->
    <!--<div class="container">-->
    <!--<div class="row">-->
    <!--<div class="section-title text-center wow zoomIn">-->
    <!--<h2>Latest <span>blog</span></h2>-->
    <!--<div class="sec_icon"><span></span><span></span><span></span></div>-->
    <!--</div>-->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">-->
    <!--<div class="home_single_blog">-->
    <!--<img class="blog-photo" alt="" src="../html-preview/keeway/assets/img/blog/1.jpg"/>-->
    <!--<div class="home_blog_text">								-->
    <!--<h4><a href="#">Boat Fisherman Fishing</a></h4>-->
    <!--<div class="post_meta"><i class="fa fa-user"></i><span>Masum Billah</span> <i class="fa fa-calendar"></i><span>12 August 2017</span></div>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed elit tortor. Quisque bibendum mauris velit.</p>-->
    <!--<a class="btn-light-bg" href="#">Read More</a>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;- END SINGLE BLOG POST &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;-->
    <!---->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">-->
    <!--<div class="home_single_blog">-->
    <!--<img class="blog-photo" alt="" src="../html-preview/keeway/assets/img/blog/2.jpg"/>-->
    <!--<div class="home_blog_text ">-->
    <!--<h4><a href="#">Adult Blur Bouquet Boy</a></h4>-->
    <!--<div class="post_meta"><i class="fa fa-user"></i><span>Masum Billah</span> <i class="fa fa-calendar"></i><span>12 August 2017</span></div>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed elit tortor. Quisque bibendum mauris velit.</p>-->
    <!--<a class="btn-light-bg" href="#">Read More</a>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;- END SINGLE BLOG POST &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;					-->
    <!---->
    <!--<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">-->
    <!--<div class="home_single_blog">-->
    <!--<img class="blog-photo" alt="" src="../html-preview/keeway/assets/img/blog/3.jpg"/>-->
    <!--<div class="home_blog_text ">-->
    <!--<h4><a href="#">Beautiful Agriculture Of Asia </a></h4>-->
    <!--<div class="post_meta"><i class="fa fa-user"></i><span>Masum Billah</span> <i class="fa fa-calendar"></i><span>12 August 2017</span></div>-->
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed elit tortor. Quisque bibendum mauris velit.</p>-->
    <!--<a class="btn-light-bg" href="#">Read More</a>-->
    <!--</div>-->
    <!--</div>&lt;!&ndash;- END SINGLE BLOG POST &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END COL &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END ROW &ndash;&gt;-->
    <!--</div>&lt;!&ndash;- END CONTAINER &ndash;&gt;-->
    <!--</section>-->
    <!--&lt;!&ndash; END HOME BLOG &ndash;&gt;-->
    <!---->
    <!---->
    <!--&lt;!&ndash; START CONTACT FORM&ndash;&gt;-->
    <section id="contact" class="contact_area section-padding">
        <div class="container">

            <div class="row">
                <div class="section-title text-center wow zoomIn">
                    <h2>Visit <span>Us</span></h2>
                    <div class="sec_icon"><span></span><span></span><span></span></div>
                </div>
                <div class="row">
                    <div class="text-center">
                        <input value="Temple Schedule" class="btn btn-lg btn-contact-bg" title="Submit Your Message!"/>
                    </div>
                    <br>
                </div>

                <div class="col-md-6">
                    <!-- map js -->
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHmo3y_PnLM9S0oFLIsAt1vq4hvlSi-bc&callback=initialize"></script>
                    <script>
                        function initialize() {
                            var mapOptions = {
                                zoom: 16,
                                scrollwheel: false,
                                center: new google.maps.LatLng(22.7181765, 75.88788890000001)
                            };
                            var map = new google.maps.Map(document.getElementById('map'),
                                mapOptions);
                            var marker = new google.maps.Marker({
                                position: map.getCenter(),
                                icon: '../html-preview/keeway/assets/img/download.png',
                                map: map
                            });
                        }
                        google.maps.event.addDomListener(window, 'load', initialize);
                    </script>

                    <div id="map"></div>
                    <!-- END MAP -->
                </div>

                <div class="col-md-3">
                    <div class="contact">
                        <form id="contact-form" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" placeholder="Your Name"
                                           required="required"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea rows="6" name="message" class="form-control" id="description"
                                              placeholder="Your Message" required="required"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <div class="actions">
                                        <input type="submit" value="Send message" name="submit" id="submitButton"
                                               class="btn btn-default btn-home-border" title="Submit Your Message!"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="contact">
                        <div class="row">
                            <div class="col-md-12 text-center"><h3 class="color4 zero-top-margin">MORE INFO</h3></div>
                            <div class="col-md-12"><h4><i class="fa fa-ban red"></i> Parking</h4></div>
                            <div class="col-md-12"><h4><i class="fa fa-ban red"></i> Photography</h4></div>
                            <div class="col-md-12"><h4><i class="fa fa-check green"></i> Differently Abled Friendly</h4>
                            </div>
                            <div class="col-md-12"><h4><i class="fa fa-ban red"></i> Daily Prasad</h4></div>
                            <div class="col-md-12"><h4><i class="fa fa-check green"></i> Special Darshan Passes</h4>
                            </div>
                            <div class="col-md-12"><h4><i class="fa fa-ban red"></i> Restroom</h4></div>
                            <div class="col-md-12"><h4><i class="fa fa-check green"></i> Shoe Stand</h4></div>
                            <div class="col-md-12"><h4><i class="fa fa-check green"></i> Locker Facility</h4></div>
                        </div>
                    </div>
                </div>

                <!-- END COL -->
            </div><!--- END ROW -->
            <br><br>
            <div class="section-title text-center wow zoomIn">
                <h2>How To <span>Reach</span></h2>
                <div class="sec_icon"><span></span><span></span><span></span></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>Go to Geeta Bhawan Malviya Nagar Metro Station and take an
                        auto/walk from there. It should take close to 5-10 mins for a walk.</h4>
                </div>
            </div>
        </div><!--- END CONTAINER -->
    </section>
    <!-- END CONTACT FORM -->

</div>
<!-- START FOOTER -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <button class="btn btn-default btn-light-bg">Add A Temple Near You</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-3 white footer_content">
                <ul>
                    <li>About</li>
                    <li>Blog</li>
                    <li>Contact</li>
                </ul>
            </div>
            <div class="col-md-4 col-xs-5 white footer_content">
                <ul>
                    <li>Privacy Policy</li>
                    <li>Terms Of Use</li>
                    <li>Payment Policies</li>
                </ul>
            </div>
            <div class="col-md-1 col-xs-1 footer_content">
                <h1>
                    <i class="fa fa-facebook-square white"></i>
                </h1>
            </div>
            <div class="col-md-1 col-xs-1 footer_content">
                <h1>
                    <i class="fa fa-instagram white"></i>
                </h1>
            </div>
            <div class="col-md-1 col-xs-1 footer_content">
                <h1>
                    <i class="fa fa-youtube white"></i>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 text-center wow zoomIn">
                <div class="footer_content white">
                    <p>Blessed.in &copy; 2018 All Rights Reserved.</p>
                </div>

            </div><!--- END COL -->
        </div><!--- END ROW -->
    </div><!--- END CONTAINER -->
</footer>
<!-- END FOOTER -->
<!-- STYLE SWITCHER -->
<!--<div id="style-switcher">-->
<!--<h2>Choose Color<a href="#"><i class="fa fa-cog fa-spin"></i></a></h2>-->
<!--<div>-->
<!--<ul class="colors" id="color1">-->
<!--<li><a href="#" class="style1"></a></li>-->
<!--<li><a href="#" class="style2"></a></li>-->
<!--<li><a href="#" class="style3"></a></li>-->
<!--<li><a href="#" class="style4"></a></li>-->
<!--<li><a href="#" class="style5"></a></li>-->
<!--<li><a href="#" class="style6"></a></li>-->
<!--<li><a href="#" class="style7"></a></li>-->
<!--<li><a href="#" class="style8"></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!-- END OF STYLE SWITCHER -->


<script type="text/javascript">
    //JQuery to fix temple name
    var fixmeTop = $('.fixme').offset().top;
    $(window).scroll(function () {
        var currentScroll = $(window).scrollTop();
        if (currentScroll >= 140) {
            $('.fixme').addClass('fix');
            $('.fixme h2').attr("style", "font-size: 24px");
            $('.fixme button').attr("style", "height: 14px");
            $('.fixme .btn-home-border').attr("style", "padding-top:6px;padding-bottom:6px;");
            //$('.fixme .btn-home-border').attr("style","padding-bottom:1px");
            $('h2.headings.color4.subheading').attr("style", "font-size: 18px");
            $('.fixme').css({
                position: 'fixed',
                top: '0',
                background: 'white',
                width: '100%',
                left: '0'

            });
        } else {

            $('.fixme').removeClass('fix');
            $('.fixme button').removeAttr("style", "height: 26px", "padding-top:10%");
            $('.fixme .btn-home-border').removeAttr("style", "padding-top:4px", "padding-bottom:4px");
            $('.fixme h2').attr("style", "font-size: 30px");
            $('.fixme').css({
                position: 'static'
            });
        }
    });
</script>
<script>
    // Open the Modal
    function openModal(id) {
        document.getElementById('myModal' + id).style.display = "block";
    }

    // Close the Modal
    function closeModal(id) {
        document.getElementById('myModal' + id).style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex, 1);

    // Next/previous controls
    function plusSlides(n, id) {
        showSlides(slideIndex += n, id);
    }

    // Thumbnail image controls
    function currentSlide(n, id) {
        showSlides(slideIndex = n, id);
    }

    function showSlides(n, id) {
        var i;
        var slides = document.getElementsByClassName("mySlides" + id);
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption" + id);
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        captionText.innerHTML = dots[slideIndex - 1].alt;
    }
    function openCity(cityName) {
        var i;
        var x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        document.getElementById(cityName).style.display = "block";
    }
</script>

</body>
</html>