<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>Blessd.in</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsive HTML5 Website Landing Page for Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- Global CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/plugins/font-awesome/css/font-awesome.css">

    <!-- github calendar css -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/plugins/github-calendar/dist/github-calendar.css"
    />
    <!-- github acitivity css -->
    <link rel="stylesheet" href="../html-preview/keeway/assets/plugins/github-activity/src/github-activity.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/octicons/2.0.2/octicons.min.css">

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="../html-preview/keeway/assets/css/styles.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<style type="text/css">
    .fa {
        font-size: .5em !important; /*size whatever you like*/
    }

    .profile_image {
        width: 300px;
        height: 300px;
    }
</style>
<!-- ******HEADER****** -->
<nav class="navbar navbar-toggleable-md navbar-light" style="background: #03a9f4">
    <p class="text-center"><h4 class="text-white">Blessed.in</h4></p>
</nav>
<header class="header">

    <div class="container">


        <img class=" ml-sm-5 profile_image  img-responsive pull-left" src="../html-preview/keeway/assets/img/profile.jpeg"
             alt="James Lee">
        <div class="profile-content pull-left">
            <h2 class="name">Pandit Neeraj Sharma</h2>
            <h5 class="desc">Pandit at Ram mandir h block vikaspuri</h5>

        </div><!--//profile-->
        <span class="btn btn-cta-secondary btn-follow pull-right follow_button" href="#" target="_blank"><i
                    class="fa fa-twitter" style="font-size: 1em !important"></i>Follow</span>
        <div class="col-sm-12 col-12 whatsapp-box">
            <form action="#" method="#" class="col-sm-10 col-10">
                <div class="form-group">
                    <input type="text" placeholder="Enter your Whatsapp Number" class="col-sm-12 col-12 form-control">
                </div>
                <button class="btn btn-primary" type="submit">Follow</button>
            </form>
        </div>
    </div><!--//container-->
</header><!--//header-->

<div class="container sections-wrapper">
    <div class="row">
        <div class="primary col-md-8 col-sm-12 col-xs-12">
            <section class="about section">
                <div class="section-inner">
                    <h2 class="heading">About Me</h2>
                    <div class="content">
                        <p>Always a keen observer of religious practices, Pt.Neeraj Sharma is a renowned astrologer who
                            has been practicing astrology for many years now. His interest in spirituality developed
                            during his brush with Gemology when he met his present Guruji who discovered the spark in
                            him and advised him to take the path he is on today. He gave it a try and his work was
                            appreciated giving him the encouragement to fuel his passion.
                        </p>


                    </div><!--//content-->
                </div><!--//section-inner-->
            </section><!--//section-->

            <!--  -->


        </div><!--//primary-->
        <div class="secondary col-md-4 col-sm-12 col-xs-12">


            <aside class="list music aside section">
                <div class="section-inner">
                    <h2 class="heading">EXPERTISE</h2>
                    <div class="content">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-circle"></i> <a href="#">Tarot</a></li>
                            <li><i class="fa fa-circle"></i> <a href="#">Astrology</a></li>
                            <li><i class="fa fa-circle"></i> <a href="#"> Numerology</a></li>
                            <li><i class="fa fa-circle"></i> <a href="#">Crystal Healing</a></li>
                            <li><i class="fa fa-circle"></i> <a href="#">Reiki</a></li>
                            <li><i class="fa fa-circle"></i> <a href="#">Past-Life Regression</a></li>
                            <li><i class="fa fa-circle"></i> <a href="#">Poojas and hawans</a></li>
                        </ul>
                    </div><!--//content-->
                </div><!--//section-inner-->
            </aside><!--//section-->


        </div><!--//content-->
    </div><!--//section-inner-->
    </aside><!--//section-->

</div><!--//secondary-->
</div><!--//row-->
</div><!--//masonry-->

<div class="secondary col-md-4 col-sm-12 col-xs-12">

    <aside class="list music aside section">
        <div class="section-inner">
            <h2 class="heading">REVIEWS</h2>
            <div class="content">
                <div class="row col-sm-12 col-12">
                    <div class="col-sm-3 col-3 btn btn-success">5.0</div>
                    <div class="col-sm-8 col-8 font-weight-bold">TERRIFIC</div>
                </div>
                <hr>
                <div class="col-12 col-sm-12">
                    <p class="text">Always a keen observer of religious practices, Pt.Neeraj Sharma is a renowned
                        astrologer who has been practicing astrology for many years now. His interest in spirituality
                        developed during his brush with Gemology when he met his present Guruji who discovered the spark
                        in him and advised him to take the path he is on today. He gave it a try and his work was
                        appreciated giving him the encouragement to fuel his passion. </p>
                </div>
            </div>
        </div>
    </aside>
</div>

<div class="secondary col-md-4 col-sm-12 col-xs-12">
    <aside class="list music aside section">
        <div class="section-inner">
            <div class="content">
                <form action="#" method="#">
                    <div class="form-gropu">
                        <input type="text" placeholder="Tap here to Write Review!" class="col-12 col-sm-12">
                    </div>
                    <br>
                    <button class="btn btn-primary col-12 col-sm-12">SUBMIT REVIEW</button>
                </form>
            </div>
        </div>
    </aside>
</div>
</div>
</aside>

</div>

<!-- ******FOOTER****** -->
<footer class="footer">

    </div><!--//container-->
</footer><!--//footer-->

<!-- Javascript -->
<script type="text/javascript" src="../html-preview/keeway/assets/plugins/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="../html-preview/keeway/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../html-preview/keeway/assets/plugins/jquery-rss/dist/jquery.rss.min.js"></script>
<!-- github calendar plugin -->
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/3.0.2/es6-promise.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fetch/0.10.1/fetch.min.js"></script>
<script type="text/javascript"
        src="../html-preview/keeway/assets/plugins/github-calendar/dist/github-calendar.min.js"></script>
<!-- github activity plugin -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.2/mustache.min.js"></script>
<script type="text/javascript" src="../html-preview/keeway/assets/plugins/github-activity/src/github-activity.js"></script>
<!-- custom js -->
<script type="text/javascript" src="../html-preview/keeway/assets/js/main.js"></script>
<script>
    $(document).ready(function () {
        $(".whatsapp-box").hide();
        $(".follow_button").click(function () {
            $(".follow_button").hide();
            $(".whatsapp-box").show();
        });
    });
</script>
</body>
</html>

